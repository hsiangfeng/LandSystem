var app = new Vue({
    el: '#app',
    data: {
        volume: 8.4,
        developer: 0,
        capacityShift: 1,
        electromechanical: 1.15,
        smallPublic: 1.05,
        balcony: 1.1,
        protruding: 0.125,
        bigPublic: 1.20,
        unitPrice:100,
        landDecimal: 2,
        unitPriceDecima:2,
    },
    computed: {
        businessFives: function () {
            let developerZeon = (this.developer == 0 ? 1 : this.developer);
            let capacityShiftZeon = (this.capacityShift == 0 ? 1 : this.capacityShift);
            let total = ((this.volume * developerZeon * capacityShiftZeon * this.electromechanical * this.smallPublic * this.balcony) + this.protruding) * this.bigPublic;
            console.log(total);
            return total.toFixed(this.landDecimal);
        },
        unitPriceTotal:function(){
            let total = this.unitPrice / this.businessFives;
            return total.toFixed(this.unitPriceDecima);
        }
    }

})